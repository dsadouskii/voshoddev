import Vue from 'vue'
import App from './App.vue'
import Resource from 'vue-resource'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import * as VueGoogleMaps from "vue2-google-maps";


//import css
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Resource);
Vue.use(BootstrapVue);

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyD1cCE2U1eVYCYHiMtO_60ZaITj9pTejpU",
    libraries: "places",
    region: "ru,en"
  }
});


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
